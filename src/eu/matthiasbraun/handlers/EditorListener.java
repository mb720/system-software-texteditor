package eu.matthiasbraun.handlers;


public interface EditorListener {

	void newEvent(EditorEvent editorEvent);

	// void textInserted(int caretPos, String insertedText, CodeArea editor);

}
