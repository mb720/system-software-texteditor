package eu.matthiasbraun.handlers;

import codearea.control.CodeArea;

public class EditorEvent {

	private final EventType type;
	private final String text;
	private final int caretStart;
	private final int caretEnd;
	private final CodeArea editor;

	public static class Builder {

		private final int NOT_SET = -1;
		private EventType type = EventType.UNKNOWN;
		private int caretEnd = NOT_SET;
		private int caretStart = NOT_SET;
		private String text = null;
		private CodeArea editor = null;

		public EditorEvent build() {
			return new EditorEvent(this);
		}

		public Builder caretEnd(final int caretEnd) {
			this.caretEnd = caretEnd;
			return this;
		}

		public Builder caretStart(final int caretStart) {
			this.caretStart = caretStart;
			return this;
		}

		public Builder editor(final CodeArea editor) {
			this.editor = editor;
			return this;
		}

		public Builder text(final String text) {
			this.text = text;
			return this;
		}

		public Builder type(final EventType type) {
			this.type = type;
			return this;
		}
	}

	public enum EventType {
		TEXT_INSERTED, TEXT_DELETED, UNKNOWN
	}

	private EditorEvent(final Builder builder) {
		this.type = builder.type;
		this.text = builder.text;
		this.caretStart = builder.caretStart;
		this.caretEnd = builder.caretEnd;
		this.editor = builder.editor;

	}

	public int getCaretEnd() {
		return caretEnd;
	}

	public int getCaretStart() {
		return caretStart;
	}

	public String getText() {
		return text;
	}

	public EventType getType() {
		return type;
	}

	public CodeArea getEditor() {
		return editor;
	}

}
