package eu.matthiasbraun.handlers.actions;

import java.io.File;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.matthiasbraun.FileUtil;
import eu.matthiasbraun.model.TextModel;
import eu.matthiasbraun.utils.EditorUtil;

public class SaveFile implements EventHandler<ActionEvent> {

	private static final Logger log = LoggerFactory.getLogger(SaveFile.class);

	@Override
	public void handle(final ActionEvent t) {
		// Save the content of the text model in the file
		final File file = EditorUtil.getCurrTextFile();
		final TextModel text = EditorUtil.getCurrTextModel();
		final String styleInfo = EditorUtil
				.getStyleInfoXml(text.getStyleInfo());
		final String fileContent = text.getText();
		// log.info("Saving to {}", file);
		// log.info("StyleInfo: {}", styleInfo);
		// log.info("Text: {}", fileContent);
		FileUtil.write(styleInfo + fileContent, file);
	}

}
