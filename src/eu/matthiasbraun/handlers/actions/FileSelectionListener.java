package eu.matthiasbraun.handlers.actions;

import java.io.File;

import javafx.scene.control.Tab;
import eu.matthiasbraun.model.TextModel;

/**
 * Listeners get notified when another file is selected. Also let the listeners
 * know which {@link TextModel} is associated with that file.
 * 
 * @author Matthias Braun
 * 
 */
public interface FileSelectionListener {
	/**
	 * A new file was selected in the editor.
	 * 
	 * @param file
	 *            The {@link File} that was selected for editing.
	 * @param text
	 *            The {@link TextModel} associated with {@code file}.
	 * @param tab
	 *            The {@link Tab} in which the {@code file} is displayed.
	 */
	void newFileSelected(File file, TextModel text, Tab tab);
}
