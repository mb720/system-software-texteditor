package eu.matthiasbraun.handlers.actions;

import java.io.File;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.matthiasbraun.utils.EditorUtil;

public class OpenFile implements EventHandler<ActionEvent> {
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(OpenFile.class);
	private final TabPane tabPane;

	public OpenFile(final TabPane tabPane) {
		this.tabPane = tabPane;
		// this.mainStage = mainStage;
	}

	@Override
	public void handle(final ActionEvent t) {
		final FileChooser fileChooser = new FileChooser();

		// Set extension filter
		final ExtensionFilter txtFilert = new ExtensionFilter(
				"Text files (*.txt)", "*.txt");
		final ExtensionFilter noFilter = new ExtensionFilter("All files (*.*)",
				"*.*");
		fileChooser.getExtensionFilters().addAll(txtFilert, noFilter);

		// Show open file dialog
		final File newFile = fileChooser.showOpenDialog(null);
		if (newFile == null) {
			// The user has cancelled the selection
		} else {

			// Open new tab for that file
			final Tab newTab = EditorUtil.createEditorInTab(newFile);
			tabPane.getTabs().add(newTab);
			// Make the new tab the active one as well
			tabPane.getSelectionModel().select(newTab);
			// mainStage.setTitle(newFile.getAbsolutePath());
		}

	}
}
