package eu.matthiasbraun.handlers.actions;

import java.io.File;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Tab;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.matthiasbraun.model.TextModel;
import eu.matthiasbraun.utils.EditorUtil;

public class TabEventHandler implements EventHandler<Event> {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory
			.getLogger(TabEventHandler.class);
	private final File file;
	private final Tab tab;
	private final TextModel text;

	public TabEventHandler(final File file, final Tab tab, final TextModel text) {
		this.file = file;
		this.tab = tab;
		this.text = text;
	}

	@Override
	public void handle(final Event event) {

		if (tab.isSelected()) {
			// log.info("Tab with file {} and textmodel {} is selected",
			// file.getName(), text.toString());
			EditorUtil.notifyFileSelectionListeners(file, text, tab);
		}

	}

}
