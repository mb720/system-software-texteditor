package eu.matthiasbraun.handlers.keys;

import java.io.File;

import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.scene.control.IndexRange;
import javafx.scene.input.KeyEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import codearea.control.CodeArea;
import eu.matthiasbraun.handlers.EditorEvent;
import eu.matthiasbraun.handlers.EditorListener;
import eu.matthiasbraun.model.TextModel;
import eu.matthiasbraun.piecelist.Piece;

public class KeyTypedHandler implements EventHandler<KeyEvent>, EditorListener {
	private static final Logger log = LoggerFactory
			.getLogger(KeyTypedHandler.class);
	/**
	 * Key code for backspace
	 */
	private static final int BACK_SPACE = 8;
	private static final int DELETE = 127;
	public File scratch;
	public Piece firstPiece;
	private final TextModel text;
	private final CodeArea editor;

	public KeyTypedHandler(final TextModel text, final CodeArea editor) {
		this.text = text;
		this.editor = editor;
	}

	@Override
	public void handle(final KeyEvent event) {
		final EventTarget t = event.getTarget();
		if (t instanceof CodeArea) {
			final CodeArea editor = (CodeArea) t;
			final int caretPos = editor.getCaretPosition();

			final String newChar = event.getCharacter();

			// log.info(newChar);
			final int charAsInt = newChar.charAt(0);
			// The caret would advance to characters otherwise
			if (newChar.equals("\r")) {
				// Do nothing because the KeyPressedHandler handles this
				// log.info("It's an enter (do nothing)");
				// caretPos--;
			} else if (charAsInt == BACK_SPACE) {
				// Let the KeyPressedHandler handle that
				// int from;
				// int to;
				// final IndexRange selection = editor.getSelection();
				// if (selection.getLength() > 0) {
				// from = selection.getStart();
				// to = selection.getEnd();
				// } else {
				// from = caretPos;
				// to = caretPos + 1;
				// }
				// text.delete(from, to);
			} else if (charAsInt == DELETE) {
				// Do nothing because the KeyPressedHandler handles this
			}
			// Insert only printable characters
			else if (!event.isControlDown()) {

				// log.info("{} at {}", newChar, caretPos);
				insertText(newChar, caretPos);
			}
		} else {
			log.warn("Unknown target: {}", t);
		}
	}

	@Override
	public void newEvent(final EditorEvent event) {
		if (this.editor.equals(event.getEditor())) {

			switch (event.getType()) {

			case TEXT_INSERTED:
				log.info("KeyTypedHandler registers that text was inserted in its editor");
				insertText(event.getText(), event.getCaretStart());
				break;
			case TEXT_DELETED:
				log.info("KeyTypedHandler registers that text was deleted in its editor");
				text.delete(event.getCaretStart(), event.getCaretEnd());
				break;
			default:
				// Do nothing on purpose
			}
		}

	}

	private void insertText(final String newText, int caretPos) {
		// Delete any selected text before inserting a character
		final IndexRange selectionRange = editor.getSelection();
		if (selectionRange.getLength() > 0) {
			final int selStart = selectionRange.getStart();
			final int selEnd = selectionRange.getEnd();
			text.delete(selStart, selEnd);
			// Set the caret to the start of the selection
			caretPos = selStart;
		}

		text.insert(caretPos, newText);

	}
}