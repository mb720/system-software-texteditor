package eu.matthiasbraun.handlers.keys;

import javafx.event.EventHandler;
import javafx.scene.control.IndexRange;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import codearea.control.CodeArea;
import eu.matthiasbraun.model.TextModel;
import eu.matthiasbraun.utils.EditorUtil;

public class KeyPressedHandler implements EventHandler<KeyEvent> {

	private static final Logger log = LoggerFactory
			.getLogger(KeyPressedHandler.class);
	private final TextModel text;

	public KeyPressedHandler(final TextModel text) {
		this.text = text;
	}

	@Override
	public void handle(final KeyEvent event) {
		if (event.getTarget() instanceof CodeArea) {
			final CodeArea area = (CodeArea) event.getTarget();
			final int caretPos = area.getCaretPosition();
			final KeyCode keyCode = event.getCode();
			switch (keyCode) {
			case BACK_SPACE:
			case DELETE:
				int from;
				int to;
				final IndexRange selection = area.getSelection();
				if (selection.getLength() > 0) {
					// Delete the selected text
					from = selection.getStart();
					to = selection.getEnd();
				} else {
					// Delete the next character after the caret
					from = caretPos;
					to = caretPos + 1;
				}
				text.delete(from, to);
				break;
			case ENTER:
				text.insert(caretPos, EditorUtil.LINEBREAK);
				break;
			default:
				// log.info("event code: {}", event.getCode());
			}
		}

	}
}
