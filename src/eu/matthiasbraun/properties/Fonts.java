package eu.matthiasbraun.properties;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import eu.matthiasbraun.config.AbstractPropertiesLoader;
import eu.matthiasbraun.config.KeyNotFoundException;

public class Fonts extends AbstractPropertiesLoader {

	private static final String PROP_FILE = "config/fonts.properties";
	public static final String DEFAULT_FONTS_KEY = "defaultFonts";
	public static final String DEFAULT_SIZES_KEY = "defaultSizes";
	public static final String DEFAULT_STYLES_KEY = "defaultStyles";
	private static final Fonts instance = new Fonts();

	public static List<String> get(final String key) {
		List<String> list = new ArrayList<>();
		try {
			list = instance.getList(key);
		} catch (final KeyNotFoundException e) {
			log.warn("Could not find key to the fonts", e);
		}
		return list;
	}

	@Override
	public File getPropertiesFile() {
		return new File(PROP_FILE);
	}

}
