package eu.matthiasbraun.properties;

import java.io.File;

import eu.matthiasbraun.config.AbstractPropertiesLoader;
import eu.matthiasbraun.config.KeyNotFoundException;

public class Paths extends AbstractPropertiesLoader {
	private static final String PROP_FILE_PATH = "config/paths.properties";

	public static final String ICONS_OPEN_DOC_KEY = "icons.openDoc";
	public static final String ICONS_FLOPPY_KEY = "icons.floppy";
	public static final String ICONS_NOTEPAD_KEY = "icons.notepad";
	private static Paths instance = new Paths();

	public static String get(final String key) {
		String value = null;
		try {
			value = instance.getValue(key);
		} catch (final KeyNotFoundException e) {
			log.warn("Could not find string in {}", GuiStrings.getFile(), e);
		}
		return value;

	}

	private Paths() {
	}

	@Override
	public File getPropertiesFile() {
		return new File(PROP_FILE_PATH);
	}
}
