package eu.matthiasbraun.properties;

import java.io.File;

import eu.matthiasbraun.config.AbstractPropertiesLoader;
import eu.matthiasbraun.config.KeyNotFoundException;

public class GuiStrings extends AbstractPropertiesLoader {
	private static final String PROP_FILE = "config/guiStrings.properties";

	private static GuiStrings instance = new GuiStrings();

	public static final String CLIPBOARD = "clipboard";
	public static final String COPY_DESCR = "copyDescr";
	public static final String COPY = "copy";
	public static final String PASTE = "paste";
	public static final String PASTE_DESCR = "pasteDescr";
	public static final String CUT_DESCR = "cutDescr";
	public static final String CUT = "cut";
	public static final String SAVE_DESCR = "saveDescr";
	public static final String SAVE = "save";
	public static final String OPEN_DESCR = "openDescr";
	public static final String OPEN = "open";
	public static final String FILE = "file";

	public static final String EDITOR = "editor";

	public static final String FONTS = "fonts";
	public static final String SIZE = "size";
	public static final String STYLES = "styles";

	public static final String PLAIN = "plain";
	public static final String BOLD = "bold";
	public static final String ITALIC = "italic";

	public static final String SEARCH = "search";

	public static String get(final String key) {
		String value = null;
		try {
			value = instance.getValue(key);
		} catch (final KeyNotFoundException e) {
			log.warn("Could not find key to icon", e);
		}
		return value;
	}

	/**
	 * @return Returns the properties file this class gets its values from.
	 */
	public static File getFile() {
		return instance.getPropertiesFile();
	}

	private GuiStrings() {
	}

	@Override
	public File getPropertiesFile() {
		return new File(PROP_FILE);
	}

}
