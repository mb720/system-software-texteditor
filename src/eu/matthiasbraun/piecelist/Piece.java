package eu.matthiasbraun.piecelist;

import java.io.File;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.matthiasbraun.FileUtil;
import eu.matthiasbraun.utils.EditorUtil;

public class Piece {
	// Default values for font, size and style
	public static final String DEFAULT_FONT = "monospace";
	public static final String DEFAULT_SIZE = "pt10";
	public static final Style DEFAULT_STYLE = Style.PLAIN;

	private StyleInfo styleInfo = new StyleInfo();

	private static final Logger log = LoggerFactory.getLogger(Piece.class);
	/**
	 * The length of the this piece's text
	 */
	private int textLength;
	/**
	 * The file containing this piece's text
	 */
	private File file;
	/**
	 * The start index of this piece's text in the file it links to relative to
	 * the end of the style info (this may either be the scratch file or the
	 * actual text file)
	 */
	private final int startOfText;
	public Piece next;

	public enum Style {
		PLAIN, ITALIC, BOLD

	}

	public Piece(final File textFile) {
		final String content = FileUtil.read(textFile);
		final int textStart = EditorUtil.getStartOfText(content);

		final int textLen = content.length() - textStart;

		this.textLength = textLen;
		this.file = textFile;
		this.startOfText = textStart;
		setDefaultStyle();
	}

	public Piece(final File file, final StyleInfo info, final int filePos) {
		this.startOfText = filePos;
		this.textLength = info.getLength();
		this.file = file;
		this.styleInfo = info;

	}

	/**
	 * Create a new piece of text pointing to a start position in a file.
	 * 
	 * @param textLength
	 *            The length of this piece's text.
	 * @param filePos
	 *            The position in the file where the text starts.
	 * @param file
	 *            The file where this text is stored.
	 */
	public Piece(final int textLength, final File file, final int filePos) {
		this.textLength = textLength;
		this.startOfText = filePos;
		this.file = file;
		// this.font = new Font(DEFAULT_FONT, DEFAULT_SIZE);
		setDefaultStyle();
	}

	public File getFile() {
		return file;
	}

	public int getFilePos() {
		return startOfText;
	}

	public String getFont() {
		return styleInfo.getFontName();
	}

	public int getLength() {
		return textLength;
	}

	public String getSize() {
		return styleInfo.getFontSize();
	}

	public String getStyle() {
		return styleInfo.getStyle().toString().toLowerCase(Locale.ENGLISH);
	}

	public StyleInfo getStyleInfo() {
		// Make sure the style info knows to which peace it belongs to
		this.styleInfo.setPiece(this);
		return this.styleInfo;
	}

	public String getText() {
		final String fileContent = FileUtil.read(file);
		final int contentLen = fileContent.length();
		// log.info("File content length: {}", contentLen);
		/*
		 * The position in the file where the style info ends and the content
		 * begins
		 */
		final int startOfContent = EditorUtil.getStartOfText(getFile());

		final int absoluteStartOfTextInFile = startOfContent + startOfText;
		final int endIndex = absoluteStartOfTextInFile + textLength;
		final String textOfPiece = fileContent.substring(
				absoluteStartOfTextInFile, endIndex);
		// log.info("{}: {}", toString(), textOfPiece);
		return textOfPiece;
	}

	public void increaseLength(final int length) {
		final int newLength = getLength() + length;
		setLength(newLength);

	}

	public boolean linksToScratchFile() {
		return file.getName().startsWith(PieceListText.SCRATCH_PREFIX);
	}

	public void setFile(final File file) {
		this.file = file;

	}

	public void setFont(final String font) {
		styleInfo.setFontName(font);
	}

	public void setLength(final int len) {
		textLength = len;
		// The styleinfo of this piece has to be updated as well
		this.styleInfo.setLength(len);

	}

	public void setSize(final String size) {
		this.styleInfo.setFontSize(size);

	}

	public void setStyle(final String style) {
		this.styleInfo.setStyle(style);

	}

	/**
	 * Let this piece have the style of the {@code other} piece.
	 * <p>
	 * These includes all style attributes except the length.
	 * 
	 * @param other
	 *            The {@link Piece} from which this piece gets its style.
	 */
	public void setStyleInfo(final Piece other) {
		this.setFont(other.getFont());
		this.setStyle(other.getStyle());
		this.setSize(other.getSize());
		// The piece always has the same length as the style info
		this.styleInfo.setLength(this.getLength());
	}

	@Override
	public String toString() {
		return file.getName() + "[" + startOfText + " - "
				+ (startOfText + textLength) + "]";
	}

	private void setDefaultStyle() {
		styleInfo.setFontName(DEFAULT_FONT);
		styleInfo.setFontSize(DEFAULT_SIZE);
		styleInfo.setStyle(DEFAULT_STYLE);
		styleInfo.setLength(textLength);
	}
}
