package eu.matthiasbraun.piecelist;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Joiner;

import eu.matthiasbraun.FileUtil;
import eu.matthiasbraun.StringUtil;
import eu.matthiasbraun.model.TextModel;
import eu.matthiasbraun.utils.EditorUtil;

public class PieceListText implements TextModel {

	public static final String SCRATCH_PREFIX = "scratch_";
	private static final Logger log = LoggerFactory
			.getLogger(PieceListText.class);
	private Piece firstPiece;
	private final File scratch;
	private final File textFile;

	public PieceListText(final File textFile) {
		this.textFile = textFile;
		this.scratch = getScratchFile();
	}

	/**
	 * @return Gets the pieces as a {@link List}
	 */
	public List<Piece> asList() {
		final List<Piece> pieces = new ArrayList<>();
		Piece currPiece = firstPiece;
		while (currPiece != null) {
			pieces.add(currPiece);
			currPiece = currPiece.next;
		}
		return pieces;
	}

	@Override
	public void delete(final int from, final int to) {
		log.info("Deleting from {} to {}", from, to);

		final Piece a = split(from);
		final Piece b = split(to);
		a.next = b.next;
		// log.info(this.toString());
	}

	@Override
	public List<StyleInfo> getStyleInfo() {
		// log.info(this.toString());
		final List<StyleInfo> styleInfos = new ArrayList<>();
		final List<Piece> pieces = this.asList();
		for (final Piece p : pieces) {
			final StyleInfo style = p.getStyleInfo();
			styleInfos.add(style);
		}
		// log.info(styleInfos.toString());
		return styleInfos;
	}

	/**
	 * @return Gets the text of every piece.
	 */
	@Override
	public String getText() {

		final StringBuilder builder = new StringBuilder();
		for (final Piece p : this.asList()) {
			final String text = p.getText();
			builder.append(text);
		}
		return builder.toString();
	}

	@Override
	public void init(final List<StyleInfo> styleInfo) {
		createPieces(this.textFile, styleInfo);
	}

	@Override
	public void insert(final int pos, final String text) {

		// log.info("String {}, pos {}, len {}", text, pos, text.length());

		Piece p = split(pos);

		if (!isLastPieceOnScratch(p)) {
			final Piece q = new Piece(0, scratch, getScratchTxtLength());
			q.next = p.next;
			p.next = q;
			// The inserted piece gets the style of the piece it was inserted in
			q.setStyleInfo(p);
			log.debug("Created {}", q);
			p = q;
		}
		// p is last piece on scratch file
		FileUtil.append(text, scratch);
		p.increaseLength(text.length());
	}

	@Override
	public void setFont(final int start, final int end, final String font) {
		// log.info("{} from {} to {}", font, start, end);
		// Split the pieces once at the start and once at the end of the font
		split(start);
		final Piece styleMe = split(end);
		styleMe.setFont(font);
		log.info("{}", this.toString());
	}

	@Override
	public void setSize(final int start, final int end, final String size) {
		split(start);
		final Piece styleMe = split(end);
		styleMe.setSize(size);

	}

	@Override
	public void setStyle(final int start, final int end, final String style) {
		log.info("{} from {}, to {}", style, start, end);
		split(start);
		final Piece styleMe = split(end);
		log.info("Styled piece: {}", styleMe);

		styleMe.setStyle(style);
	}

	@Override
	public String toString() {

		final List<Piece> pieces = asList();
		final String content = this.getText();
		final String piecesStr = Joiner.on(" --> ").join(pieces);
		return content + ": " + piecesStr;
	}

	/**
	 * 
	 * Parse the style info at the beginning of the file. If it exists create
	 * the necessary number of pieces with their style information
	 * 
	 * @param textFile
	 *            The file containing the text.
	 * @param styleInfo
	 *            A list of {@link StyleInfo} that determines which differently
	 *            styled pieces exist.
	 */
	private void createPieces(final File textFile,
			final List<StyleInfo> styleInfo) {

		/*
		 * Create a new empty scratch file (delete the contents of the old one
		 * if it existed)
		 */
		FileUtil.write("", scratch);
		// final String fileContent = FileUtil.read(textFile);

		// The first piece points to the empty scratch file
		firstPiece = new Piece(scratch);
		// The file has no recognized style information
		if (styleInfo.isEmpty()) {
			// The file with the actual text is the second piece
			firstPiece.next = new Piece(textFile);
		} else {
			/*
			 * Create a piece for each piece in the style info (from the
			 * beginning of the file)
			 */
			Piece currPiece = firstPiece;
			// The position in the scratch file where the piece starts
			int filePos = 0;

			final String textFileContent = FileUtil.read(textFile);
			final int endOfStyleInfo = EditorUtil.getStartOfText(textFile);
			for (final StyleInfo info : styleInfo) {
				/*
				 * Write the text stored in the text file into the scratch file.
				 * Otherwise if we later on write text from the scratch file
				 * into the text file this would cause the piece to point to the
				 * wrong position in the text file.
				 */
				final int startOfTextInTextFile = endOfStyleInfo + filePos;
				final String textFromTextFile = textFileContent.substring(
						startOfTextInTextFile,
						startOfTextInTextFile + info.getLength());
				FileUtil.append(textFromTextFile, scratch);

				final Piece p = new Piece(scratch, info, filePos);
				info.setPiece(p);

				currPiece.next = p;
				currPiece = p;
				// The next piece starts at this position
				filePos += info.getLength();
			}
		}
		// log.info("{}", toString());
	}

	/**
	 * Every text file gets a helper scratch file in its directory where text
	 * entered by the user is stored.
	 * 
	 * @return The location of the scratch file as a {@link File}.
	 */
	private File getScratchFile() {

		final String textFileName = FileUtil.getFileName(textFile
				.getAbsolutePath());
		final String scratchFileName = SCRATCH_PREFIX + textFileName;
		// final String textFileDir = textFile.getParent();
		final String textFileDir = FileUtil.getParent(textFile);
		return FileUtil.join(textFileDir, scratchFileName);
	}

	private int getScratchTxtLength() {
		final int charCount = FileUtil.getCharCount(scratch);
		final String contest = FileUtil.read(scratch);
		final int numberOfEnters = StringUtil.count('\n', contest);
		// log.info("Found {} enters.", numberOfEnters);
		return charCount - numberOfEnters;
	}

	private boolean isLastPieceOnScratch(final Piece piece) {
		final boolean pointsToScratch = piece.linksToScratchFile();

		final int filePos = piece.getFilePos();
		final int pieceLen = piece.getLength();
		final int scratchTxtLength = getScratchTxtLength();
		final boolean textIsLastInScratch = filePos + pieceLen >= scratchTxtLength;
		return pointsToScratch && textIsLastInScratch;
	}

	/**
	 * Split the text into two pieces at {@code pos}.
	 * <p>
	 * If a piece ends at {@code pos} no new piece is created and this piece is
	 * returned instead.
	 * 
	 * @param pos
	 *            The position where a split is made.
	 * @return The piece left of {@code pos}.
	 */
	private Piece split(final int pos) {
		if (pos == 0) {
			return firstPiece;
		}
		// set p to the piece containing pos
		Piece p = firstPiece;
		// The end position of the left piece
		int leftEnd = p.getLength();
		while (pos > leftEnd && p.next != null) {
			p = p.next;
			leftEnd = leftEnd + p.getLength();
		}
		// --- split piece p
		if (pos != leftEnd) {
			final int rightLen = leftEnd - pos;
			final int newLeftLen = p.getLength() - rightLen;
			p.setLength(newLeftLen);// shorten the piece
			final Piece q = new Piece(rightLen, p.getFile(), p.getFilePos()
					+ newLeftLen);
			// The new piece gets the style of the old piece
			q.setStyleInfo(p);

			log.info("Created {}", q);
			q.next = p.next;
			p.next = q;
		}
		return p;
	}

}
