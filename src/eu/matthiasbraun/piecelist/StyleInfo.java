package eu.matthiasbraun.piecelist;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import eu.matthiasbraun.XmlUtil;
import eu.matthiasbraun.piecelist.Piece.Style;

/**
 * A style info tells us what the size, font and style of a text range is.
 * 
 * @author Matthias Braun
 * 
 */
public class StyleInfo {

	private static final Logger log = LoggerFactory.getLogger(StyleInfo.class);
	/**
	 * XML tags concerning the style found in the text files.
	 */
	public static final String STYLE = "style", FONT = "font",
			LENGTH = "length", SIZE = "size", PIECE = "piece",
			STYLE_INFO = "styleInfo";

	// Default values for font, size and style
	public static final String DEFAULT_FONT = "monospace";
	public static final String DEFAULT_SIZE = "pt10";
	public static final Style DEFAULT_STYLE = Style.PLAIN;

	private String fontName = DEFAULT_FONT;

	private Style style = DEFAULT_STYLE;
	private String fontSize = DEFAULT_SIZE;
	private int length;

	private Piece piece;

	public static StyleInfo copyOf(final StyleInfo currInfo) {
		final StyleInfo copy = new StyleInfo();
		copy.setFontName(currInfo.getFontName());
		copy.setFontSize(currInfo.getFontSize());
		copy.setStyle(currInfo.getStyle());
		copy.setLength(currInfo.getLength());
		return copy;
	}

	public static void main(final String[] args) {
		final String text = "\n\r";
		int nr = 0;
		if (text.contains("\n")) {
			nr++;
		}
		if (text.contains("\r")) {
			nr++;
		}
		log.info("{}", nr);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final StyleInfo other = (StyleInfo) obj;
		if (fontName == null) {
			if (other.fontName != null) {
				return false;
			}
		} else if (!fontName.equals(other.fontName)) {
			return false;
		}
		if (fontSize == null) {
			if (other.fontSize != null) {
				return false;
			}
		} else if (!fontSize.equals(other.fontSize)) {
			return false;
		}
		if (length != other.length) {
			return false;
		}
		if (style != other.style) {
			return false;
		}
		return true;
	}

	public String getFontName() {
		return fontName;
	}

	public String getFontSize() {
		return fontSize;
	}

	public int getLength() {
		return this.length;
	}

	public int getNrOfInvisibleChars() {
		int nr = 0;
		final String text = piece.getText();
		if (text.contains("\n")) {
			nr++;
		}
		if (text.contains("\r")) {
			nr++;
		}
		return nr;
	}

	public String getStyle() {
		return style.toString().toLowerCase();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((fontName == null) ? 0 : fontName.hashCode());
		result = prime * result
				+ ((fontSize == null) ? 0 : fontSize.hashCode());
		result = prime * result + length;
		result = prime * result + ((style == null) ? 0 : style.hashCode());
		return result;
	}

	/**
	 * Parse the style information from a {@link Node} and set them in this
	 * object.
	 * 
	 * @param node
	 *            The {@link Node} containing the style information.
	 */
	public void parseVals(final Node node) {
		final List<Node> kids = XmlUtil.asList(node.getChildNodes());
		for (final Node n : kids) {
			final String propName = n.getNodeName();
			// Get what's between the opening and the closing tag
			final String val = n.getTextContent();
			switch (propName) {
			case LENGTH:
				length = Integer.parseInt(val);
				break;
			case FONT:
				fontName = val;
				break;
			case SIZE:
				fontSize = val;
				break;
			case STYLE:
				style = parseStyle(val);
				break;
			default:
				log.warn("Unknown style property {} with value {}", propName,
						val);
				break;
			}
		}

	}

	public void setFontName(final String fontName) {
		this.fontName = fontName;
	}

	public void setFontSize(final String fontSize) {
		this.fontSize = fontSize;
	}

	public void setLength(final int length) {
		// log.info("Setting length of style info {} to {}", this, length);
		this.length = length;

	}

	public void setPiece(final Piece p) {
		this.piece = p;

	}

	public void setStyle(final String style) {
		setStyle(parseStyle(style));

	}

	public void setStyle(final Style style) {
		this.style = style;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append(LENGTH + ": ").append(length + "; ");
		builder.append(FONT + ": ").append(fontName + "; ");
		builder.append(STYLE + ": ").append(style + "; ");
		builder.append(SIZE + ": ").append(fontSize);
		return builder.toString();
	}

	private Style parseStyle(final String val) {
		final Style parsedStyle;
		switch (val) {
		case "plain":
			parsedStyle = Style.PLAIN;
			break;
		case "bold":
			parsedStyle = Style.BOLD;
			break;
		case "italic":
			parsedStyle = Style.ITALIC;
			break;
		default:
			// If the style is unknown, interpret it as plain
			parsedStyle = Style.PLAIN;
			break;
		}
		return parsedStyle;
	}

}
