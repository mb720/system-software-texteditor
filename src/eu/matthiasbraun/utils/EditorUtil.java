package eu.matthiasbraun.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import codearea.control.CodeArea;
import eu.matthiasbraun.FileUtil;
import eu.matthiasbraun.XmlUtil;
import eu.matthiasbraun.handlers.EditorEvent;
import eu.matthiasbraun.handlers.EditorListener;
import eu.matthiasbraun.handlers.actions.FileSelectionListener;
import eu.matthiasbraun.handlers.actions.TabEventHandler;
import eu.matthiasbraun.handlers.keys.KeyPressedHandler;
import eu.matthiasbraun.handlers.keys.KeyTypedHandler;
import eu.matthiasbraun.model.TextModel;
import eu.matthiasbraun.piecelist.Piece;
import eu.matthiasbraun.piecelist.PieceListText;
import eu.matthiasbraun.piecelist.StyleInfo;
import eu.matthiasbraun.properties.Fonts;

public final class EditorUtil implements FileSelectionListener {

	private static final Logger log = LoggerFactory.getLogger(EditorUtil.class);

	public static final List<String> fonts = Fonts.get(Fonts.DEFAULT_FONTS_KEY);
	public static final List<String> sizes = Fonts.get(Fonts.DEFAULT_SIZES_KEY);
	public static final List<String> styles = Fonts
			.get(Fonts.DEFAULT_STYLES_KEY);
	/**
	 * A file may contain information about the fonts and styles of the text.
	 * This strings marks the end of that information.
	 */
	private static final String STYLE_INFO_CLOSING_TAG = "</styleInfo>";
	private static final String PIECE_TAG = "piece";

	private static File currTextFile;

	private static EditorUtil instance = new EditorUtil();

	private static TextModel currTextModel;

	private final static List<EditorListener> editorListeners = new ArrayList<>();
	private final static List<FileSelectionListener> fileSelectionListeners = new ArrayList<>();

	/**
	 * This character represents a linebreak.
	 */
	public static final String LINEBREAK = "௹";

	static {
		/*
		 * The EditorUtil is the place where clients can get the currently
		 * selected text file and the text model associated with it
		 */
		addFileSelectionListener(instance);
	}

	public static void addEditorListener(final EditorListener listener) {
		editorListeners.add(listener);

	}

	public static void addFileSelectionListener(
			final FileSelectionListener listener) {
		fileSelectionListeners.add(listener);
	}

	/**
	 * Style the text in the editor according to the style info that was stored
	 * in the file
	 * 
	 * @param styleInfo
	 *            A list {@link StyleInfo}s which determine the look of the text
	 *            (font, size, style).
	 * @param editor
	 *            The editor containing the text that is styled.
	 */
	public static void applyStyleToText(final List<StyleInfo> styleInfo,
			final CodeArea editor) {
		int start = 0;

		for (final StyleInfo info : styleInfo) {
			final int end = start + info.getLength();
			final String fontName = fontDisplayNameToCssName(info.getFontName());
			final String style = info.getStyle();
			final String size = info.getFontSize();
			setFont(start, end, fontName, editor);
			setSize(start, end, size, editor);
			setStyle(start, end, style, editor);
			start = end;
		}

	}

	public static Tab createEditorInTab(final CodeArea editor,
			final TextModel text, final File file) {
		/*
		 * JavaFx distinguishes between pressed/released and typed keys. The
		 * KeyTypedHandler gets all the typed in strings but not the backspace
		 * key. This is why we need the KeyPressedHandler which receives all the
		 * non printable characters like backspace and delete.
		 */
		editor.setOnKeyPressed(new KeyPressedHandler(text));
		final KeyTypedHandler keyTypedHandler = new KeyTypedHandler(text,
				editor);
		editor.setOnKeyTyped(keyTypedHandler);
		addEditorListener(keyTypedHandler);

		// Put the editor inside a tab
		final Tab tab = new Tab();
		tab.setContent(editor);
		// The tab knows when it is selected and what file and text it harbors
		tab.setOnSelectionChanged(new TabEventHandler(file, tab, text));
		tab.setText(file.getName());
		return tab;
	}

	public static Tab createEditorInTab(final File txtFile) {
		final List<StyleInfo> styleInfo = getStyleInfo(txtFile);
		final TextModel text = EditorUtil.getTextModel(styleInfo, txtFile);
		final CodeArea editor = new CodeArea(text.getText());

		return createEditorInTab(editor, text, txtFile);
		// final TextModel text = EditorUtil.getTextModel(styleInfo, file);
		// final String content = FileUtil.read(file);
		// final CodeArea editor = new CodeArea(content);
		//
		// return createEditorInTab(editor, text, file);
	}

	/**
	 * 
	 * Translate the font from its GUI representation to the corresponding CSS
	 * property (i.e., replace spaces with underscores and make everything
	 * lowercase).
	 * 
	 * @param displayName
	 *            The font name as shown to the user.
	 * @return The name of the font in the CSS.
	 */
	public static String fontDisplayNameToCssName(final String displayName) {
		return displayName.replace(" ", "_").toLowerCase(Locale.ENGLISH);
	}

	public static File getCurrTextFile() {
		return currTextFile;
	}

	public static TextModel getCurrTextModel() {
		return currTextModel;
	}

	/**
	 * Get the currently selected editor from a {@link TabPane} which might have
	 * multiple tabs with one editor each.
	 * 
	 * @param tabPane
	 *            {@link} TabPane that contains all tabs.
	 * @throws ClassCastException
	 *             This method is supposed to throw a classcast exception if the
	 *             tab does not contain a {@link CodeArea}.
	 * @return The {@link CodeArea} of the currently selected tab.
	 */
	public static CodeArea getSelectedEditor(final TabPane tabPane) {
		return (CodeArea) tabPane.getSelectionModel().getSelectedItem()
				.getContent();
	}

	/**
	 * @see #getStartOfText(String)
	 * @param textFile
	 * @return
	 */
	public static int getStartOfText(final File textFile) {
		return getStartOfText(FileUtil.read(textFile));
	}

	/**
	 * A text file can have information about text style and fonts at its
	 * beginning. Get the position in the file after this information. This is
	 * the start of the file's text.
	 * <p>
	 * If the file does not contain the {@linkplain #STYLE_INFO_CLOSING_TAG
	 * style information closing} tag {@code 0} is returned.
	 * 
	 * @param fileContentWithStyleInfo
	 *            The file's content that may include style information.
	 * @return The start of the file's text. This is the position after the
	 *         style info if it exists and {@code 0} if it doesn't.
	 */
	public static int getStartOfText(final String fileContentWithStyleInfo) {
		final int startOfClosingTag = fileContentWithStyleInfo
				.indexOf(STYLE_INFO_CLOSING_TAG);
		final int startOfText;
		// The file doesn't contain style information
		if (startOfClosingTag == -1) {
			startOfText = 0;
		} else {

			startOfText = startOfClosingTag + STYLE_INFO_CLOSING_TAG.length();
		}
		return startOfText;
	}

	// public static void notifyEditorListeners(final int caretPos,
	// final String insertedText, final CodeArea editor) {
	// for (final EditorListener listener : instance.editorListeners) {
	// listener.textInserted(caretPos, insertedText, editor);
	// }
	// }

	/**
	 * Parses a list of {@link StyleInfo} from a string in XML format.
	 * <p>
	 * If the file does not contain any style info, an empty {@link List} is
	 * returned.
	 * 
	 * @param styleInfoStr
	 *            The string in XML containing the style information.
	 * @return A list of {@link StyleInfo} parsed from {@code styleInfoStr}.
	 */
	public static List<StyleInfo> getStyleInfo(final File file) {
		final List<StyleInfo> styleInfo = new ArrayList<>();
		final String fileContent = FileUtil.read(file);
		final int startOfText = getStartOfText(fileContent);
		if (startOfText != 0) {
			// The file contains style info
			final String styleInfoStr = fileContent.substring(0, startOfText);
			final List<Node> nodes = XmlUtil.getNodes(styleInfoStr, PIECE_TAG);
			for (final Node node : nodes) {

				final StyleInfo styleInfoPiece = new StyleInfo();
				styleInfoPiece.parseVals(node);
				styleInfo.add(styleInfoPiece);

			}
		}
		return styleInfo;
	}

	/**
	 * Create a style info in XML format from the individual {@link StyleInfo}s
	 */
	public static String getStyleInfoXml(final List<StyleInfo> styleInfos) {
		final StringBuilder builder = new StringBuilder();
		final String modelStr = getCurrTextModel().toString();
		log.info(modelStr);
		final int preStyleInfosLen = getCombinedLength(styleInfos);
		final int mergedStyleInfosLen = getCombinedLength(styleInfos);
		final int piecesLen = getCombinedLength(getCurrTextModel());
		log.info("Styles: {}, Merged: {}, Pieces: {}", preStyleInfosLen,
				mergedStyleInfosLen, piecesLen);
		final List<StyleInfo> mergedInfos = mergeStyleInfos(styleInfos);
		for (final StyleInfo info : mergedInfos) {
			final String lengthElem = XmlUtil.createElement(StyleInfo.LENGTH,
					info.getLength());
			final String fontElem = XmlUtil.createElement(StyleInfo.FONT,
					info.getFontName());
			final String styleElem = XmlUtil.createElement(StyleInfo.STYLE,
					info.getStyle());
			final String sizeElem = XmlUtil.createElement(StyleInfo.SIZE,
					info.getFontSize());
			// Wrap all the style info of one piece into one XML element
			builder.append(XmlUtil.createElement(StyleInfo.PIECE, lengthElem
					+ fontElem + styleElem + sizeElem));
		}
		final String newModelStr = getCurrTextModel().toString();
		if (!newModelStr.equals(modelStr)) {
			log.warn("Model was changed");
		}
		// Wrap all the pieces into one single XML element
		return XmlUtil.createElement(StyleInfo.STYLE_INFO, builder.toString());
	}

	public static TextModel getTextModel(final List<StyleInfo> styleInfo,
			final File textFile) {
		final TextModel text = new PieceListText(textFile);
		text.init(styleInfo);

		return text;
	}

	public static void notifyEditorListeners(final EditorEvent editorEvent) {

		for (final EditorListener listener : editorListeners) {
			listener.newEvent(editorEvent);
		}
	}

	public static void notifyFileSelectionListeners(final File file,
			final TextModel text, final Tab tab) {
		for (final FileSelectionListener listener : fileSelectionListeners) {
			listener.newFileSelected(file, text, tab);
		}
	}

	public static void setFont(final int start, final int end,
			final String newFont, final CodeArea editor) {
		// If this is not a Set the whole line will be styled
		Set<String> currStyles = new HashSet<>(editor.getStyleAt(start));
		int beginCurrStyles = start;
		for (int i = start; i < end; i++) {
			final Set<String> newStyles = new HashSet<>(editor.getStyleAt(i));

			/*
			 * In this range is a change in style or size. Only change the font
			 * not the style or the size
			 */
			if (!newStyles.equals(currStyles)) {
				// Reset all font but not the style or the size
				currStyles.removeAll(fonts);
				currStyles.add(newFont);
				editor.setStyle(beginCurrStyles, i, currStyles);
				beginCurrStyles = i;
				currStyles = newStyles;
			}
		}
		/*
		 * Style the last piece of text (which may be the whole range if it has
		 * the same size and style from start to end).
		 */
		if (beginCurrStyles != end) {
			// Reset all fonts but not the style or the size
			currStyles.removeAll(fonts);
			currStyles.add(newFont);
			// log.info("Applying {} from {} to {}", currStyles,
			// beginCurrStyles,
			// end);
			// editor.setStyleClasses(beginCurrStyles, end, currStyles);
			editor.setStyle(beginCurrStyles, end, currStyles);
		}
	}

	public static void setSize(final int start, final int end,
			final String newSize, final CodeArea editor) {
		// Bug in CodeArea: If this is not a Set the whole line will be styled
		Set<String> currStyles = new HashSet<>(editor.getStyleAt(start));
		int beginCurrStyles = start;
		for (int i = start; i < end; i++) {
			final Set<String> newStyles = new HashSet<>(editor.getStyleAt(i));
			/*
			 * In this range is a change in style or font. Only change the size
			 * not the style or the font
			 */
			if (!newStyles.equals(currStyles)) {
				// Reset all font but not the style or the size
				currStyles.removeAll(sizes);
				currStyles.add(newSize);
				editor.setStyle(beginCurrStyles, i, currStyles);
				beginCurrStyles = i;
				currStyles = newStyles;
			}
		}
		/*
		 * Style the last piece of text (which may be the whole range if it has
		 * the same font and style from start to end).
		 */
		if (beginCurrStyles != end) {
			// Reset all fonts but not the style or the size
			currStyles.removeAll(sizes);
			currStyles.add(newSize);
			// log.info("Applying {} from {} to {}", currStyles,
			// beginCurrStyles,
			// end);
			// editor.setStyleClasses(beginCurrStyles, end, currStyles);
			editor.setStyle(beginCurrStyles, end, currStyles);
		}
	}

	/**
	 * Sets the style from {@code start} to {@code end} in this {@code editor}
	 * to a {@code newStyle}.
	 * <p>
	 * The implementation of {@link CodeArea#setStyleClass(int, int, String)}
	 * overwrites all other style classes which is not always desirable.
	 * 
	 * 
	 * @param start
	 *            The start of the text where the style should be set.
	 * @param end
	 *            The end of the text where the style should be set.
	 * @param newStyle
	 *            The style to apply to the text.
	 * @param editor
	 *            The {@link CodeArea} which contains the text to be styled.
	 */
	public static void setStyle(final int start, final int end,
			final String newStyle, final CodeArea editor) {
		// If this is not a Set the whole line will be styled
		Set<String> currStyles = new HashSet<>(editor.getStyleAt(start));
		int beginCurrStyles = start;
		for (int i = start; i < end; i++) {
			final Set<String> newStyles = new HashSet<>(editor.getStyleAt(i));

			/*
			 * In this range is a change in font or size. Only change the style
			 * not the font or the size
			 */
			if (!newStyles.equals(currStyles)) {
				// Reset all styles but not the font or the size
				currStyles.removeAll(styles);
				currStyles.add(newStyle);
				editor.setStyle(beginCurrStyles, i, currStyles);
				beginCurrStyles = i;
				currStyles = newStyles;
			}

		}
		/*
		 * Style the last piece of text (which may be the whole range if it has
		 * the same size and font from start to end).
		 */
		if (beginCurrStyles != end) {
			// Reset all styles but not the font or the size
			currStyles.removeAll(styles);
			currStyles.add(newStyle);
			// log.info("Applying {} from {} to {}", currStyles,
			// beginCurrStyles,
			// end);
			// editor.setStyleClasses(beginCurrStyles, end, currStyles);
			editor.setStyle(beginCurrStyles, end, currStyles);
		}
	}

	private static int getCombinedLength(final List<StyleInfo> styleInfos) {
		int combinedLength = 0;
		for (final StyleInfo info : styleInfos) {
			combinedLength += info.getLength();
		}
		return combinedLength;
	}

	private static int getCombinedLength(final TextModel currTextModel2) {
		int combinedLength = 0;
		final PieceListText list = (PieceListText) currTextModel2;
		for (final Piece p : list.asList()) {
			combinedLength += p.getLength();
		}
		return combinedLength;
	}

	private static List<StyleInfo> mergeStyleInfos(
			final List<StyleInfo> styleInfos) {
		final List<StyleInfo> mergedInfos = new ArrayList<>();
		/*
		 * If two or more adjacent pieces have the same style (font, size, etc.)
		 * merge those styles
		 */
		int currStyleIndex = 0;
		while (currStyleIndex < styleInfos.size()) {
			final StyleInfo currInfo = styleInfos.get(currStyleIndex);
			if (currInfo.getLength() > 0) {
				final StyleInfo mergedInfo = StyleInfo.copyOf(currInfo);
				while (styleInfosAreEqual(styleInfos, currStyleIndex)) {
					final StyleInfo nextInfo = styleInfos
							.get(currStyleIndex + 1);
					/*
					 * Extend the length of the current info because its equal
					 * to its neighbor
					 */
					final int combinedLength = mergedInfo.getLength()
							+ nextInfo.getLength();
					mergedInfo.setLength(combinedLength);
					// Try to merge the next style info
					currStyleIndex++;
				}
				// The current style and its neighbor were not equal
				mergedInfos.add(mergedInfo);
			}
			currStyleIndex++;
		}
		return mergedInfos;
	}

	/**
	 * Compare if a style info and its right neighbor are equal
	 * <p>
	 * Compare each style attribute with the exception of the style's length.
	 * This is used to merge two equivalent style infos.
	 */
	private static boolean styleInfosAreEqual(final List<StyleInfo> styleInfos,
			final int currIndex) {
		boolean infosAreEqual = false;

		// The current index and the next index must be in bounds
		if (currIndex < styleInfos.size() - 1) {
			final StyleInfo info1 = styleInfos.get(currIndex);
			final StyleInfo info2 = styleInfos.get(currIndex + 1);
			final boolean fontsAreEqual = info1.getFontName().equals(
					info2.getFontName());
			final boolean stylesAreEqual = info1.getStyle().equals(
					info2.getStyle());
			final boolean fontSizesAreEqual = info1.getFontSize() == info2
					.getFontSize();
			infosAreEqual = fontsAreEqual && stylesAreEqual
					&& fontSizesAreEqual;
		}
		return infosAreEqual;
	}

	@Override
	public void newFileSelected(final File file, final TextModel text,
			final Tab tab) {
		currTextFile = file;
		currTextModel = text;

		applyStyleToText(currTextModel.getStyleInfo(),
				(CodeArea) tab.getContent());
	}
}
