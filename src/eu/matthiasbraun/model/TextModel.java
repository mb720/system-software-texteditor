package eu.matthiasbraun.model;

import java.util.List;

import eu.matthiasbraun.piecelist.StyleInfo;

public interface TextModel {
	public void delete(int from, int to);

	public List<StyleInfo> getStyleInfo();

	public String getText();

	public void init(List<StyleInfo> styleInfo);

	public void insert(int caretPos, String newText);

	public void setFont(int start, int end, String font);

	public void setSize(int start, int end, String size);

	public void setStyle(int start, int end, String style);

}
