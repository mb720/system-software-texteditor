package application;

import java.io.File;
import java.util.List;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.IndexRange;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import codearea.control.CodeArea;

import com.google.common.base.Optional;

import eu.matthiasbraun.FileUtil;
import eu.matthiasbraun.StringUtil;
import eu.matthiasbraun.SysUtil;
import eu.matthiasbraun.handlers.EditorEvent;
import eu.matthiasbraun.handlers.EditorEvent.EventType;
import eu.matthiasbraun.handlers.actions.OpenFile;
import eu.matthiasbraun.handlers.actions.SaveFile;
import eu.matthiasbraun.model.TextModel;
import eu.matthiasbraun.piecelist.PieceListText;
import eu.matthiasbraun.piecelist.StyleInfo;
import eu.matthiasbraun.properties.GuiStrings;
import eu.matthiasbraun.properties.Paths;
import eu.matthiasbraun.utils.EditorUtil;

public class Main extends Application {
	private static final int DEFAULT_HEIGHT = 500;
	private static final int DEFAULT_WIDTH = 500;
	private static final String TEXT_STYLES_CSS = "/res/styles/textStyles.css";
	private static final String DEFAULT_FILE = "test.txt";
	private static final Logger log = LoggerFactory
			.getLogger(PieceListText.class);

	// At this position the search for string starts
	private int startOfSearch = 0;
	/**
	 * The number returned when {@link String#indexOf(String)} doesn't find the
	 * searched string.
	 */
	protected static final int STRING_NOT_FOUND_INDEX = -1;

	public static void main(final String[] args) {
		launch(args);
	}

	public void buildGui(final Stage stage) {
		stage.setTitle(GuiStrings.get(GuiStrings.EDITOR));
		stage.setWidth(DEFAULT_WIDTH);
		stage.setHeight(DEFAULT_HEIGHT);
		stage.getIcons().add(new Image(Paths.get(Paths.ICONS_NOTEPAD_KEY)));

		final File file = new File(DEFAULT_FILE);

		/*
		 * A file may start with information about how the text should be styled
		 * in terms of fonts and size
		 */
		final String contentWithStyleInfo = FileUtil.read(file);
		// Don't show the style info in the editor
		final int textStart = EditorUtil.getStartOfText(contentWithStyleInfo);
		// Text without style info
		final String textWithMarkup = contentWithStyleInfo.substring(textStart);
		final String pureText = textWithMarkup.replaceAll(EditorUtil.LINEBREAK,
				"\n");
		final CodeArea editor = new CodeArea(pureText);

		final List<StyleInfo> styleInfos = EditorUtil.getStyleInfo(file);
		// The user can edit many files, each shown in an editor in its own tab
		final TextModel textModel = EditorUtil.getTextModel(styleInfos, file);
		final Tab tab = EditorUtil.createEditorInTab(editor, textModel, file);
		// EditorUtil.applyStyleToText(styleInfos, editor);
		final TabPane tabPane = new TabPane();
		tabPane.getTabs().add(tab);

		final MenuBar menuBar = createMenuBar(stage, tabPane);

		// The borderpane contains all open editors as tabs and a menubar on top
		final BorderPane borderPane = new BorderPane();
		borderPane.setTop(menuBar);
		borderPane.setCenter(tabPane);
		borderPane.setBottom(createSearchBar(editor));

		final Scene scene = new Scene(borderPane);

		/*
		 * Add the CSS containing styles (bold, italic) and fonts. Don't forget
		 * to refresh the project when changing the CSS. Otherwise the old
		 * version is used.
		 */
		scene.getStylesheets().add(
				Main.class.getResource(TEXT_STYLES_CSS).toExternalForm());

		stage.setScene(scene);
		stage.show();
	}

	@Override
	public void start(final Stage stage) {
		buildGui(stage);
	}

	private void copyTextToClipboard(final TabPane tabPane) {
		final Optional<CodeArea> editor = getEditor(tabPane);

		if (editor.isPresent()) {
			final String selectedText = editor.get().getSelectedText();
			SysUtil.copyToClipboard(selectedText);

		} else {
			log.warn("Could not copy text to clipboard because editor couldn't be found");
		}

	}

	private Menu createClipboardMenu(final TabPane tabPane) {
		final Menu clipboardMenu = new Menu(
				GuiStrings.get(GuiStrings.CLIPBOARD));
		final MenuItem copyItem = new MenuItem(GuiStrings.get(GuiStrings.COPY));
		copyItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(final ActionEvent event) {
				copyTextToClipboard(tabPane);
			}
		});
		final MenuItem pasteItem = new MenuItem(
				GuiStrings.get(GuiStrings.PASTE));
		pasteItem.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(final ActionEvent event) {
				final Optional<CodeArea> editorMaybe = getEditor(tabPane);
				if (editorMaybe.isPresent()) {
					final CodeArea editor = editorMaybe.get();
					final String insertThis = SysUtil.getClipboardString();
					final int caretPos = editor.getCaretPosition();
					editor.insertText(caretPos, insertThis);

					// Let the text model know that text was inserted
					final EditorEvent newEvent = new EditorEvent.Builder()
							.type(EventType.TEXT_INSERTED).caretStart(caretPos)
							.text(insertThis).editor(editor).build();
					EditorUtil.notifyEditorListeners(newEvent);

				} else {
					log.warn("Could not get text editor");
				}
			}

		});
		final MenuItem cutItem = new MenuItem(GuiStrings.get(GuiStrings.CUT));
		cutItem.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(final ActionEvent event) {
				copyTextToClipboard(tabPane);
				final Optional<CodeArea> editorMaybe = getEditor(tabPane);
				if (editorMaybe.isPresent()) {
					// Delete the selected text in the editor
					final CodeArea editor = editorMaybe.get();
					final int start = editor.getSelection().getStart();
					final int end = editor.getSelection().getEnd();
					editor.replaceText(start, end, "");

					// Delete the selected text in the text model
					final EditorEvent editorEvent = new EditorEvent.Builder()
							.type(EventType.TEXT_DELETED).caretStart(start)
							.caretEnd(end).editor(editor).build();
					EditorUtil.notifyEditorListeners(editorEvent);
				}

			}
		});
		clipboardMenu.getItems().addAll(copyItem, pasteItem, cutItem);
		return clipboardMenu;
	}

	private Menu createFileMenu(final EventHandler<ActionEvent> saveFile,
			final EventHandler<ActionEvent> openFile) {
		final Menu fileMenu = new Menu(GuiStrings.get(GuiStrings.FILE));
		// Create the Save File item
		final MenuItem saveItem = new MenuItem(GuiStrings.get(GuiStrings.SAVE));
		final String iconPath = Paths.get(Paths.ICONS_FLOPPY_KEY);
		final Image saveImg = new Image(iconPath);
		saveItem.setGraphic(new ImageView(saveImg));
		saveItem.setOnAction(saveFile);
		// Create the Open File item
		final MenuItem openItem = new MenuItem(GuiStrings.get(GuiStrings.OPEN));
		final Image openImg = new Image(Paths.get(Paths.ICONS_OPEN_DOC_KEY));
		openItem.setGraphic(new ImageView(openImg));
		openItem.setOnAction(openFile);
		fileMenu.getItems().addAll(saveItem, openItem);
		return fileMenu;
	}

	private Menu createFontMenu(final TabPane tabPane) {
		final Menu fontMenu = new Menu(GuiStrings.get(GuiStrings.FONTS));
		// Add the default fonts to the dropdown box
		// fontMenu.setItems(FXCollections.observableArrayList(defaultFonts));
		for (final String font : EditorUtil.fonts) {

			final MenuItem fontItem = new MenuItem(font);
			fontMenu.getItems().add(fontItem);
			fontItem.setOnAction(new EventHandler<ActionEvent>() {

				/*
				 * Change the font of the selected text when the user chooses a
				 * new font
				 */
				@Override
				public void handle(final ActionEvent arg0) {
					final CodeArea editor = EditorUtil
							.getSelectedEditor(tabPane);
					final IndexRange selection = editor.getSelection();
					// Only change font if there is a selection
					if (selection.getLength() > 0) {
						final String fontInCss = EditorUtil
								.fontDisplayNameToCssName(font);
						final int start = selection.getStart();
						final int end = selection.getEnd();
						// Save the style info in the piece
						EditorUtil.getCurrTextModel().setFont(start, end,
								fontInCss);
						// Make the style visible in the editor
						EditorUtil.setFont(start, end, fontInCss, editor);
						log.info("Apply {} from {} to {}", fontInCss, start,
								end);

					}
				}
			});
		}
		return fontMenu;

	}

	private MenuBar createMenuBar(final Stage stage, final TabPane tabPane) {
		final MenuBar menuBar = new MenuBar();
		/*
		 * The 'save file'-command queries what text to save in which file from
		 * the EditorUtil
		 */
		final SaveFile saveFile = new SaveFile();
		final OpenFile openFile = new OpenFile(tabPane);
		final Menu fileMenu = createFileMenu(saveFile, openFile);
		final Menu clipboardMenu = createClipboardMenu(tabPane);
		final Menu styleMenu = createStyleMenu(tabPane);
		final Menu fontMenu = createFontMenu(tabPane);
		final Menu sizeMenu = createSizeMenu(tabPane);

		menuBar.getMenus().addAll(fileMenu, clipboardMenu, styleMenu, fontMenu,
				sizeMenu);
		// The menu bar expands with the rest of the window
		menuBar.prefWidthProperty().bind(stage.widthProperty());
		return menuBar;
	}

	private Node createSearchBar(final CodeArea editor) {
		final HBox searchBar = new HBox();
		final TextField searchField = new TextField();
		searchField.setPromptText(GuiStrings.get(GuiStrings.SEARCH));
		searchField.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(final ActionEvent arg0) {
				searchInEditor(editor, searchField.getText(), false);
			}

		});
		final Button next = new Button(">");
		next.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(final ActionEvent arg0) {
				searchInEditor(editor, searchField.getText(), false);
			}
		});
		final Button prev = new Button("<");
		prev.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(final ActionEvent arg0) {
				searchInEditor(editor, searchField.getText(), true);
			}
		});
		searchBar.getChildren().addAll(searchField, prev, next);
		return searchBar;
	}

	private Menu createSizeMenu(final TabPane tabPane) {
		final Menu sizeMenu = new Menu(GuiStrings.get(GuiStrings.SIZE));
		for (final String size : EditorUtil.sizes) {
			final MenuItem styleItem = new MenuItem(size);
			styleItem.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(final ActionEvent arg0) {
					final CodeArea editor = EditorUtil
							.getSelectedEditor(tabPane);

					final IndexRange selection = editor.getSelection();
					// Only change size if there is a selection
					if (selection.getLength() > 0) {
						final int start = selection.getStart();
						final int end = selection.getEnd();

						// Make the size visible in the editor
						EditorUtil.setSize(start, end, size, editor);
						// Save the size info in the piece
						EditorUtil.getCurrTextModel().setSize(start, end, size);
						log.info("Apply {} from {} to {}", size, start, end);

					}
				}
			});

			sizeMenu.getItems().add(styleItem);
		}
		return sizeMenu;
	}

	private Menu createStyleMenu(final TabPane tabPane) {
		final Menu styleMenu = new Menu(GuiStrings.get(GuiStrings.STYLES));
		for (final String style : EditorUtil.styles) {
			final MenuItem styleItem = new MenuItem(GuiStrings.get(style));
			styleItem.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(final ActionEvent arg0) {
					final CodeArea editor = EditorUtil
							.getSelectedEditor(tabPane);
					final IndexRange selection = editor.getSelection();
					// Only change style if there is a selection
					if (selection.getLength() > 0) {
						final int start = selection.getStart();
						final int end = selection.getEnd();

						// Make the style visible in the editor
						EditorUtil.setStyle(start, end, style, editor);
						// Save the style info in the piece
						EditorUtil.getCurrTextModel().setStyle(start, end,
								style);
						// editor.setStyleClass(start, end, style);
						log.info("Apply {} from {} to {}", style, start, end);

					}
				}
			});

			styleMenu.getItems().add(styleItem);
		}
		return styleMenu;
	}

	private Optional<CodeArea> getEditor(final TabPane tabPane) {

		CodeArea editor = null;
		final Tab selectedTab = tabPane.getSelectionModel().getSelectedItem();
		final Node tabContent = selectedTab.getContent();
		if (tabContent instanceof CodeArea) {
			editor = (CodeArea) tabContent;
		} else {
			log.warn("Tab content is not a CodeArea, it's {}",
					tabContent.getClass());

		}
		return Optional.fromNullable(editor);
	}

	private void searchInEditor(final CodeArea editor, final String searchStr,
			final boolean backwards) {
		final String text = editor.getText();
		int startOfSearchStrInText;
		// Search the string backwards in the text
		if (backwards) {
			int startSearchHere;
			if (startOfSearch == 0) {
				/*
				 * The string was not found before, continue searching form the
				 * start
				 */
				startSearchHere = startOfSearch;
			} else {
				startSearchHere = text.length() - startOfSearch
						+ searchStr.length();
			}
			/*
			 * Revert both the text and the string to search in the opposite
			 * direction
			 */
			final String reversedTxt = StringUtil.reverse(text);
			final String reversedSearchStr = StringUtil.reverse(searchStr);
			startOfSearchStrInText = reversedTxt.indexOf(reversedSearchStr,
					startSearchHere);
		} else {

			startOfSearchStrInText = text.indexOf(searchStr, startOfSearch);
		}
		if (startOfSearchStrInText != STRING_NOT_FOUND_INDEX) {

			if (backwards) {
				startOfSearchStrInText = text.length() - startOfSearchStrInText
						- searchStr.length();
			}
			final int endOfSearchStrInText = startOfSearchStrInText
					+ searchStr.length();
			// Highlight the found text
			editor.selectRange(startOfSearchStrInText, endOfSearchStrInText);
			/*
			 * When the user searches the term another time the next occurrence
			 * is searched
			 */
			startOfSearch = endOfSearchStrInText;
		} else {
			// Continue the search at the beginning
			startOfSearch = 0;
		}
	}

}
